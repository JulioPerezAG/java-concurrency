package io.japg.javaconcurrency.services;

import io.japg.javaconcurrency.models.post.Post;
import io.japg.javaconcurrency.models.user.User;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Log4j2
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {

    private final RestTemplate restTemplate;

    public User findUser(int userId) throws ExecutionException, InterruptedException {
        log.info("Current Thread: {}", Thread.currentThread().getName());

        return CompletableFuture.supplyAsync(() -> {
                    log.info("runAsync - Current Thread: {}", Thread.currentThread().getName());
                    return restTemplate
                            .getForObject("https://jsonplaceholder.typicode.com/posts?userId=" + userId, Post[].class);
                }).thenApply(posts -> {
                    log.info("thenApply - Current Thread: {}", Thread.currentThread().getName());
                    return Arrays.stream(posts).toList();
                }).thenApplyAsync(posts -> {
                    log.info("thenApplyAsync - Current Thread: {}", Thread.currentThread().getName());
                    User user = restTemplate.getForObject("https://jsonplaceholder.typicode.com/users/" + userId, User.class);
                    user.setPosts(posts);
                    return user;
                })
                .get();
    }

    public User findUserInComposedWay(int userId) throws ExecutionException, InterruptedException {
        log.info("Current Thread: {}", Thread.currentThread().getName());

        CompletableFuture<List<Post>> completableFuturePosts = CompletableFuture.supplyAsync(() -> {
            log.info("runAsync - Current Thread: {}", Thread.currentThread().getName());
            return restTemplate
                    .getForObject("https://jsonplaceholder.typicode.com/posts?userId=" + userId, Post[].class);
        }).thenApply(posts -> {
            log.info("thenApply - Current Thread: {}", Thread.currentThread().getName());
            return Arrays.stream(posts).toList();
        });

        CompletableFuture<User> completableFutureUser = CompletableFuture.supplyAsync(() -> {
                    log.info("thenApplyAsync - Current Thread: {}", Thread.currentThread().getName());
                    return restTemplate.getForObject("https://jsonplaceholder.typicode.com/users/" + userId, User.class);
                });

        CompletableFuture.allOf(completableFuturePosts, completableFutureUser)
                .join();

        User user = completableFutureUser.join();
        user.setPosts(completableFuturePosts.join());
        return user;
    }
}
