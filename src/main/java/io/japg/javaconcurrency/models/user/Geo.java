package io.japg.javaconcurrency.models.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Geo {
    private double lat;
    private double lng;
}
