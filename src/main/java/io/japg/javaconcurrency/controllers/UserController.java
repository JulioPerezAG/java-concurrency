package io.japg.javaconcurrency.controllers;

import io.japg.javaconcurrency.models.user.User;
import io.japg.javaconcurrency.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    private final UserService userService;

    @GetMapping("/v1/users/{userId}")
    public User getUserV1(@PathVariable int userId) throws ExecutionException, InterruptedException {
        return userService.findUser(userId);
    }

    @GetMapping("/v2/users/{userId}")
    public User getUserV2(@PathVariable int userId) throws ExecutionException, InterruptedException {
        return userService.findUserInComposedWay(userId);
    }
}
