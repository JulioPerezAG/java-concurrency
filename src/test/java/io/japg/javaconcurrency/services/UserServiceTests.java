package io.japg.javaconcurrency.services;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.ExecutionException;

@SpringBootTest
public class UserServiceTests {

    @Autowired
    private UserService userService;

    @Test
    public void testFindUser() throws ExecutionException, InterruptedException {
        int userId = 1;
        userService.findUser(userId);
    }

    @Test
    public void testFindUserInComposedWay() throws ExecutionException, InterruptedException {
        int userId = 1;
        userService.findUserInComposedWay(userId);
    }
}
